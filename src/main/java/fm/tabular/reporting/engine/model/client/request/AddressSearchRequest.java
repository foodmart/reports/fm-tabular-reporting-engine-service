package fm.tabular.reporting.engine.model.client.request;

import fm.tabular.reporting.engine.model.type.StringType;
import lombok.Data;

@Data
public class AddressSearchRequest {

    private StringType storeCity;

    private StringType storeCountry;

    private StringType storeManager;

    private StringType storePostalCode;

    private StringType storeState;

    private StringType storeStreetAddress;
}
