package fm.tabular.reporting.engine.model.client.request;

import fm.tabular.reporting.engine.model.type.StringType;
import fm.tabular.reporting.engine.model.type.Type;
import lombok.Data;

@Data
public class PositionsSearchRequest {

    private StringType positionTitle;

    private StringType paymentType;

    private Type<Integer> minScale;

    private Type<Integer> maxScale;

    private StringType managementRole;
}
