package fm.tabular.reporting.engine.model.client.request;

import fm.tabular.reporting.engine.model.type.StringType;
import fm.tabular.reporting.engine.model.type.Type;
import lombok.Data;

@Data
public class RegionSearchRequest {

    private StringType salesCity;

    private StringType salesCityProvince;

    private StringType salesDistrict;

    private StringType salesRegions;

    private StringType salesCountry;

    private Type<Integer> salesDistrictId;
}
