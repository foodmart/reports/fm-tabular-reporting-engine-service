package fm.tabular.reporting.engine.model.table;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataTable {

    private List<String> headers;

    private int size;

    private List<Row> rows;

}
