package fm.tabular.reporting.engine.model.client.request;

import lombok.Data;

@Data
public class SupervisorSearchRequest {

    private String fullName;
}
