package fm.tabular.reporting.engine.model.client.request;

import fm.tabular.reporting.engine.model.type.StringType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentsSearchRequest {

    private StringType departmentDescription;

}
