package fm.tabular.reporting.engine.model.client.request;

import fm.tabular.reporting.engine.model.type.StringType;
import lombok.Data;

@Data
public class ContactSearchRequest {

    private StringType storeFax;

    private StringType storePhone;

}
