package fm.tabular.reporting.engine.model.client.request;

import fm.tabular.reporting.engine.model.enums.Gender;
import fm.tabular.reporting.engine.model.enums.MartialStatus;
import fm.tabular.reporting.engine.model.type.LocalDateTimeType;
import fm.tabular.reporting.engine.model.type.StringType;
import fm.tabular.reporting.engine.model.type.Type;
import jakarta.validation.Valid;
import lombok.Data;

@Data
public class EmployeeSearchRequest {

    private @Valid StringType fullName;

    private @Valid StringType firstName;

    private @Valid StringType lastName;

    private @Valid Type<Integer> positionId;

    private @Valid StringType positionTitle;

    private @Valid Type<Integer> storeId;

    private @Valid Type<Integer> departmentId;

    private @Valid LocalDateTimeType birthDate;

    private @Valid LocalDateTimeType hireDate;

    private @Valid LocalDateTimeType endDate;

    private @Valid Type<Double> salary;

    private @Valid Type<Long> supervisorId;

    private @Valid StringType educationLevel;

    private @Valid Type<MartialStatus> maritalStatus;

    private @Valid Type<Gender> gender;

    private @Valid StringType managementRole;

    /*

     */
}
