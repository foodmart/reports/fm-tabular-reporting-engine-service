package fm.tabular.reporting.engine.model.type;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
public class Type<T> {

    @NotNull
    private T value;

}
