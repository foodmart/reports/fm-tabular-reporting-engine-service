package fm.tabular.reporting.engine.model.enums;

public enum Gender {
    M, F
}
