package fm.tabular.reporting.engine.model.client.request;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchRequest {

    private @Valid EmployeeSearchRequest employee;

    private PositionsSearchRequest position;

    private SupervisorSearchRequest supervisor;

    private DepartmentsSearchRequest department;

    private StoresSearchRequest store;

}
