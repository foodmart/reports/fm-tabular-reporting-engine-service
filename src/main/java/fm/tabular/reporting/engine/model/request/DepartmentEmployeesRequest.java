package fm.tabular.reporting.engine.model.request;

import fm.tabular.reporting.engine.model.type.StringType;
import lombok.Data;

@Data
public class DepartmentEmployeesRequest {

    private StringType departmentDescription;
}
