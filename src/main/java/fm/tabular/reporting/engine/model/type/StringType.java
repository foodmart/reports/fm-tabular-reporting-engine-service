package fm.tabular.reporting.engine.model.type;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.validation.annotation.Validated;

@Data
@EqualsAndHashCode(callSuper = true)
@Validated
public class StringType extends Type<String> {

    private boolean exact;

}
