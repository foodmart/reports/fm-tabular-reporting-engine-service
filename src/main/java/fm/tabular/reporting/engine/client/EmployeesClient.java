package fm.tabular.reporting.engine.client;

import fm.common.core.model.annoation.CommonParams;
import fm.common.core.model.response.ResponseBodyEntity;
import fm.tabular.reporting.engine.model.client.request.SearchRequest;
import fm.tabular.reporting.engine.model.client.response.EmployeesResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(url = "${config.feign.fm-employees-processing-service}", name = "fm-employees-processing-service")
public interface EmployeesClient {

    @PostMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
    @CommonParams
    ResponseBodyEntity<EmployeesResponse> findEmployees(@RequestBody SearchRequest searchRequest, @RequestParam("page") int page, @RequestParam("size") int size);
}
