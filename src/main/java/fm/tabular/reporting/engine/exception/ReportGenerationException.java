package fm.tabular.reporting.engine.exception;

public class ReportGenerationException extends RuntimeException {

    public ReportGenerationException(String message) {
        super(message);
    }
}
