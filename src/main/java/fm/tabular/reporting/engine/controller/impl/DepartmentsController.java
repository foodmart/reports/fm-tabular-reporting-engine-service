package fm.tabular.reporting.engine.controller.impl;

import fm.tabular.reporting.engine.controller.doc.DepartmentsDoc;
import fm.tabular.reporting.engine.model.request.DepartmentEmployeesRequest;
import fm.tabular.reporting.engine.service.reporting.DepartmentsReportingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class DepartmentsController implements DepartmentsDoc {

    private final DepartmentsReportingService departmentsReportingService;

    @Override
    public ResponseEntity<byte[]> generateDepartmentEmployees(DepartmentEmployeesRequest request) {
        byte[] data = departmentsReportingService.generateDepartmentEmployees(request);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        headers.setContentDispositionFormData("filename", request.getDepartmentDescription() + "-employees.pdf");
        headers.setPragma("public");
        headers.setExpires(0L);
        return ResponseEntity.status(HttpStatus.CREATED)
                .headers(headers)
                .body(data);
    }
}
