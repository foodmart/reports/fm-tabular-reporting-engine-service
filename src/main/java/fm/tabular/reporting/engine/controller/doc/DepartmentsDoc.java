package fm.tabular.reporting.engine.controller.doc;

import fm.tabular.reporting.engine.model.request.DepartmentEmployeesRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@RequestMapping("/departments")
public interface DepartmentsDoc {

    @PostMapping(value = "/employees", produces = APPLICATION_JSON_VALUE)
    ResponseEntity<byte[]> generateDepartmentEmployees(@RequestBody DepartmentEmployeesRequest request);


}
