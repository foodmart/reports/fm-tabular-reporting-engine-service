package fm.tabular.reporting.engine.service.reporting;


import fm.tabular.reporting.engine.model.request.DepartmentEmployeesRequest;

public interface DepartmentsReportingService {

    byte[] generateDepartmentEmployees(DepartmentEmployeesRequest request);


}
