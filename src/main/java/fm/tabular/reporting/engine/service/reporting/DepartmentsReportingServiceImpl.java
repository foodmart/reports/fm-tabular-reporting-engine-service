package fm.tabular.reporting.engine.service.reporting;

import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.TextAlignment;
import fm.tabular.reporting.engine.exception.DataNotFoundException;
import fm.tabular.reporting.engine.model.client.response.EmployeeResponse;
import fm.tabular.reporting.engine.model.report.ReportContent;
import fm.tabular.reporting.engine.model.request.DepartmentEmployeesRequest;
import fm.tabular.reporting.engine.model.table.DataTable;
import fm.tabular.reporting.engine.model.table.Row;
import fm.tabular.reporting.engine.service.cell.CellService;
import fm.tabular.reporting.engine.service.client.EmployeesService;
import fm.tabular.reporting.engine.service.table.TableService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DepartmentsReportingServiceImpl implements DepartmentsReportingService {

    private final EmployeesService employeesService;

    private final TableService tableService;

    private final CellService cellService;

    private final ReportGenerator reportGenerator;


    @Override
    public byte[] generateDepartmentEmployees(DepartmentEmployeesRequest request) {
        List<EmployeeResponse> employees = employeesService.findEmployees(request).getEmployees();
        if (employees.isEmpty()) {
            throw new DataNotFoundException("There is no data to generate report.");
        }
        List<Row> rows = employees.stream().map(employee ->
                Row.builder()
                        .cells(cellService.create(String.valueOf(employee.getEmployeeId()), employee.getFullName(), employee.getPositionTitle()))
                        .build()
        ).toList();
        List<String> headers = List.of("Employee Id", "Full Name", "Position Title");

        Table dataTable = tableService.create(DataTable.builder()
                .headers(headers)
                .size(3)
                .rows(rows)
                .build());
        Table criteriaTable = tableService.create(DataTable.builder()
                        .size(2)
                        .rows(List.of(Row.builder()
                                .cells(cellService.create("Department description", request.getDepartmentDescription().getValue()))
                                .build()))
                        .build())
                .setMargin(10);

        Paragraph title = new Paragraph("Department's Employees").setTextAlignment(TextAlignment.CENTER).setMargin(10);
        return reportGenerator.generate(ReportContent.builder()
                .paragraphs(List.of(title))
                .tables(List.of(criteriaTable, dataTable))
                .build());
    }
}
