package fm.tabular.reporting.engine.service.cell;

import fm.tabular.reporting.engine.model.table.Cell;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class CellServiceImpl implements CellService {

    @Override
    public List<Cell> create(String... elements) {
        return Arrays.stream(elements)
                .map(element -> Cell.builder().content(element).build())
                .toList();
    }
}
