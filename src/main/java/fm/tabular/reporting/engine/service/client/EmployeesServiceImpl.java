package fm.tabular.reporting.engine.service.client;

import fm.tabular.reporting.engine.client.EmployeesClient;
import fm.tabular.reporting.engine.config.AppConfig;
import fm.tabular.reporting.engine.model.client.request.DepartmentsSearchRequest;
import fm.tabular.reporting.engine.model.client.request.SearchRequest;
import fm.tabular.reporting.engine.model.client.response.EmployeeResponse;
import fm.tabular.reporting.engine.model.client.response.EmployeesResponse;
import fm.tabular.reporting.engine.model.request.DepartmentEmployeesRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeesServiceImpl implements EmployeesService {

    private final EmployeesClient employeesClient;

    private final AppConfig appConfig;

    @Override
    public EmployeesResponse findEmployees(DepartmentEmployeesRequest request) {
        final int batchSize = appConfig.getBatchSize();
        SearchRequest searchRequest = SearchRequest.builder()
                .department(DepartmentsSearchRequest.builder()
                        .departmentDescription(request.getDepartmentDescription())
                        .build())
                .build();
        int page = 0;
        EmployeesResponse employeesResponse = employeesClient.findEmployees(searchRequest, page, batchSize)
                .getData();
        List<EmployeeResponse> employees = new ArrayList<>(employeesResponse.getEmployees());
        while (!employeesResponse.getEmployees().isEmpty()) {
            page++;
            employeesResponse = employeesClient.findEmployees(searchRequest, page, batchSize)
                    .getData();
            employees.addAll(employeesResponse.getEmployees());
        }
        return EmployeesResponse.builder().employees(employees).build();
    }
}
