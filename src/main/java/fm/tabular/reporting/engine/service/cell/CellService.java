package fm.tabular.reporting.engine.service.cell;

import fm.tabular.reporting.engine.model.table.Cell;

import java.util.List;

public interface CellService {

    List<Cell> create(String... elements);
}
