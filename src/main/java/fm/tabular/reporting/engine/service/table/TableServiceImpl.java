package fm.tabular.reporting.engine.service.table;

import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import fm.tabular.reporting.engine.model.table.DataTable;
import org.springframework.stereotype.Service;

@Service
public class TableServiceImpl implements TableService {
    @Override
    public Table create(DataTable dataTable) {
        Table table = new Table(UnitValue.createPercentArray(dataTable.getSize())).useAllAvailableWidth();
        if (dataTable.getHeaders() != null) {
            dataTable.getHeaders().forEach(header -> table.addCell(new Cell().add(new Paragraph(header)).setTextAlignment(TextAlignment.CENTER)));
        }
        if (dataTable.getRows() != null) {
            dataTable.getRows().forEach(row -> row.getCells().forEach(cell -> table.addCell(new Cell().add(new Paragraph(cell.getContent()))
                    .setTextAlignment(TextAlignment.CENTER))));
        }
        return table;
    }
}
