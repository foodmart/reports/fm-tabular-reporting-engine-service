package fm.tabular.reporting.engine.service.reporting;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import fm.tabular.reporting.engine.model.report.ReportContent;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;

@Service
public class ReportGeneratorImpl implements ReportGenerator {
    @Override
    public byte[] generate(ReportContent reportContent) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        PdfWriter writer = new PdfWriter(bos);
        PdfDocument pdf = new PdfDocument(writer);
        Document document = new Document(pdf);
        reportContent.getParagraphs().forEach(document::add);
        reportContent.getTables().forEach(document::add);
        document.close();
        return bos.toByteArray();
    }
}
