package fm.tabular.reporting.engine.service.client;

import fm.tabular.reporting.engine.model.client.response.EmployeesResponse;
import fm.tabular.reporting.engine.model.request.DepartmentEmployeesRequest;

public interface EmployeesService {

    EmployeesResponse findEmployees(DepartmentEmployeesRequest request);

}
