package fm.tabular.reporting.engine.service.table;

import com.itextpdf.layout.element.Table;
import fm.tabular.reporting.engine.model.table.DataTable;

public interface TableService {

    Table create(DataTable dataTable);


}
