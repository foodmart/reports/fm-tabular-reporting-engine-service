package fm.tabular.reporting.engine.service.reporting;

import fm.tabular.reporting.engine.model.report.ReportContent;

public interface ReportGenerator {

    byte[] generate(ReportContent reportContent);

}
